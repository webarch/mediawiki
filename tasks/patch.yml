# Copyright 2018-2025 Chris Croome
#
# This file is part of the Webarchitects MediaWiki Ansible role.
#
# The Webarchitects MediaWiki Ansible role is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
#
# The Webarchitects Ansible role is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along with the Webarchitects MediaWiki Ansible role. If not, see <https://www.gnu.org/licenses/>.
---
- name: Download and apply a MediaWiki patch
  block:

    - name: Download the MediaWiki public GPG keys
      ansible.builtin.get_url:
        url: "https://www.mediawiki.org/keys/keys.txt"
        dest: "{{ mediawiki_private }}/mediawiki.asc"
        force: true
        mode: "0600"
        owner: "{{ mediawiki_user }}"
        group: "{{ mediawiki_group }}"

    - name: Import MediaWiki public GPG keys
      ansible.builtin.command: "gpg --no-tty --import {{ mediawiki_private }}/mediawiki.asc"
      register: mediawiki_gpg_import
      changed_when:
        - ( "unchanged" not in mediawiki_gpg_import.stderr_lines | last )
        - ( "not changed" not in mediawiki_gpg_import.stderr_lines | last )
      become: true
      become_user: "{{ mediawiki_user }}"

    - name: Download MediaWiki patch file
      ansible.builtin.get_url:
        url: "https://releases.wikimedia.org/mediawiki/{{ mediawiki_version_installed }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch.gz"
        dest: "{{ mediawiki_private }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch.gz"
        mode: "0600"
        owner: "{{ mediawiki_user }}"
        group: "{{ mediawiki_group }}"

    - name: Download MediaWiki patch file sig
      ansible.builtin.get_url:
        url: "https://releases.wikimedia.org/mediawiki/{{ mediawiki_version_installed }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch.gz.sig"
        dest: "{{ mediawiki_private }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch.gz.sig"
        mode: "0600"
        owner: "{{ mediawiki_user }}"
        group: "{{ mediawiki_group }}"

    - name: Check MediaWiki GPG signature
      ansible.builtin.command: "gpg --no-tty --verify --logger-fd 1 mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch.gz.sig"
      args:
        chdir: "{{ mediawiki_private }}"
      check_mode: false
      changed_when: false
      register: mediawiki_patch_signature_check
      failed_when: >-
        ( mediawiki_patch_signature_check.rc != 0 ) or
        ( "Good signature" not in mediawiki_patch_signature_check.stdout )
      become: true
      become_user: "{{ mediawiki_user }}"

    - name: Unarchive MediaWiki patch
      ansible.builtin.command: "gunzip mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch.gz"
      args:
        chdir: "{{ mediawiki_private }}"
        creates: "{{ mediawiki_private }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch"
      changed_when: true
      become: true
      become_user: "{{ mediawiki_user }}"
      when: ( "Good signature" in mediawiki_patch_signature_check.stdout )

    - name: Test patch in a block to catch errors
      block:

        - name: Test the patch with dry-run  # noqa: command-instead-of-module
          ansible.builtin.command: "patch -p1 --dry-run -i {{ mediawiki_private }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch"
          args:
            chdir: "{{ mediawiki_docroot }}/w"
          check_mode: false
          changed_when: false
          register: mediawiki_patch_dryrun
          become: true
          become_user: "{{ mediawiki_user }}"

      rescue:

        - name: Debug the failure stdout
          ansible.builtin.debug:
            var: mediawiki_patch_dryrun.stdout_lines

        - name: Debug patch failure
          ansible.builtin.debug:
            msg: >-
              The patch dry run failed, see the stdout above. If this failure was only for a
              minor problem, like the Ansible modified images/.htaccess file then it should
              be safe to patch using force with this following commands as the {{ mediawiki_user }}
              user.

        - name: Debug patch failure force commands
          ansible.builtin.debug:
            msg:
              - "su - {{ mediawiki_user }}"
              - "cd {{ mediawiki_docroot }}/w"
              - "patch -p1 --force -i {{ mediawiki_private }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch"

        - name: Fail due to patch dry-run failing
          ansible.builtin.fail:

    - name: Apply the patch  # noqa: command-instead-of-module
      ansible.builtin.command: "patch -p1 -i {{ mediawiki_private }}/mediawiki-{{ mediawiki_version_installed }}.{{ mediawiki_version_patch_proposed }}.patch"
      args:
        chdir: "{{ mediawiki_docroot }}/w"
      changed_when: true
      become: true
      become_user: "{{ mediawiki_user }}"
      when: mediawiki_patch_dryrun.rc == 0

  tags:
    - mediawiki
...
