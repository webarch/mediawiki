# Webarchitects Ansible MediaWiki Role

[![pipeline status](https://git.coop/webarch/mediawiki/badges/master/pipeline.svg)](https://git.coop/webarch/mediawiki/-/commits/master)

Ansible [MediaWiki](https://www.mediawiki.org/wiki/MediaWiki) role to install and then patch version 1.35.0 or greater.

This role has been designed to work with the [Webarchitects Secure Hosting](https://git.coop/webarch/wsh) example repo and all the roles that uses, this role is used by the [users role](https://git.coop/webarch/users).

Some assumptions that this role makes would ideally be set via the [defaults](defaults/main.yml) but for now they are hard coded:

1. No public account creation, no public editing, so the admin users needs to create accounts for others users, this is to prevent spam being posted to wiki sites.
2. [Short URLs](https://www.mediawiki.org/wiki/Manual:Short_URL/Apache) in the form `https://example.org/wiki/Main_Page`.

There is support for using the [patch upgrade](https://www.mediawiki.org/wiki/Manual:Upgrading#Using_patch) however this is not suitable for MediaWiki sites with complex customations or extensions or signon systems, for these MediaWiki sites and for major upgrades between versions it is best to install a new MediaWiki site alongside the existing one and then import the database, copy the files and run [the update script](https://www.mediawiki.org/wiki/Manual:Upgrading#Run_the_update_script).

This role was completely rewritten in December 2019, [the version from before the rewrite can be browsed here](https://git.coop/webarch/mediawiki/tree/8af384bcddd0cb1e93dc67855ee61e9f044ea4d9).

## Notes

Check the version of MediaWiki:

```bash
php maintenance/version.php | awk '{ print $3 }' | jc --semver -yp
```

```yaml
---
major: 1
minor: 35
patch: 6
prerelease:
build:
```

## Copyright

Copyright 2018-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
